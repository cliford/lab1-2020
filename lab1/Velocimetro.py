#! /usr/bin/env python3
# -*- coding:utf-8 -*-


class Velocimetro():

    # generacion de self
    def __init__(self, velocimetro):
        self.velocimetro = velocimetro

    # Pedir a usuariola velocidad
    def get_velocimetro(self):
        velocidad = int(input("Ingrese velocidad del auto: "))
        self.velocimetro = velocidad
        return self.velocimetro
