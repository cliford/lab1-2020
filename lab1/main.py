#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from Auto import Auto
import random


def est_auto(motor, estanque, rueda1, rueda2, rueda3, rueda4, velocimetro):
    # generar texto, con estado actual del auto(la nave)
    print("----------- Estado de la nave -------------------")
    print("El motor es de {0} cilintradas".format(motor))
    print("El estanque es de {0} Lt".format(estanque))
    print("La velocidad del auto es {0}".format(0))
    print("El estado de las ruedas son:")
    print(("R1: {0} % | R2: {1} % | R3: {2} % | R4: {3} %")
          .format(rueda1, rueda2, rueda3, rueda4))
    print("--------------------------------------------------\n")


def desgaste_rueda(rueda1, rueda2, rueda3, rueda4):
    # generar desgaste individual para cada rueda(random)
    desr1 = random.randint(1, 10)
    desr2 = random.randint(1, 10)
    desr3 = random.randint(1, 10)
    desr4 = random.randint(1, 10)

    # aplicar desgaste a la rueda
    # rueda 1
    if rueda1 <= 10:
        rueda1 = 100
    else:
        rueda1 = rueda1 - desr1

    # rueda 2
    if rueda2 <= 10:
        rueda2 = 100
    else:
        rueda2 = rueda2 - desr2

    # rueda 3
    if rueda3 <= 10:
        rueda3 = 100
    else:
        rueda3 = rueda3 - desr3

    # rueda 4
    if rueda4 <= 10:
        rueda4 = 100
    else:
        rueda4 = rueda4 - desr4

    return rueda1, rueda2, rueda3, rueda4


def consumo_de_combustible(motor, estanque, velocidad):
    # generar tiempo aleatorio entre 1 y 10 horas
    tiempo = random.randint(1, 10)
    # formula de distancia utilizada
    distancia = velocidad * tiempo

    # generar consumo, demostrar tiempo recorrido y distancia
    # para cara vehiculo
    # 1.2/ 20km*L
    # 1.6/ 14km*L

    if motor == "1.2":
        consumo = distancia/20
        print("Tiempo recorrido: {0} Hr".format(tiempo))
        print("Distancia recorrida {0} Km, consumo: {1} L\n".format
              (distancia, consumo))
    else:
        consumo = distancia/14
        print("Tiempo recorrido: {0} Hr".format(tiempo))
        print("Distancia recorrida {0} Km, consumo: {1} L\n".format
              (distancia, consumo))

    # aplicar el consumo al estanque
    estanque = estanque - consumo
    return estanque


def Encender(motor, estanque, rueda1, rueda2, rueda3, rueda4):
    # generar llave para encender auto o apagarlo
    llave = int(input('Presine el botón "0": y para apagar el "9": '))
    if llave == 0:
        # mostar estado en reposo
        print("\n-----------Estado inicial-------------------")
        print("El motor es de {0} cilintradas".format(motor))
        print("El estanque es de {0} Lt".format(estanque))
        print("La velocidad del auto es {0}".format(0))
        print("El estado de las ruedas son:")
        print(("R1: {0} % | R2: {1} % | R3: {2} % | R4: {3} %")
              .format(rueda1, rueda2, rueda3, rueda4))
        print("----------------------------------------------\n")

        # reducir el 1% de arrancada al estanque
        estanque = estanque - (estanque/100)
        return True, estanque

    # apagado en primea instancia
    elif llave == 9:
        print("Aweonao, te subiste al auto por las puras")

    # datos erroneos
    else:
        print("Error al ingresar datos reinicie aplicacion")


if __name__ == "__main__":

    # variables utilizadas en todo el programa
    motor = None
    estanque = None
    rueda1 = None
    rueda2 = None
    rueda3 = None
    rueda4 = None
    velocimetro = None
    final = True

    # generar objeto Nave con clase Auto archivo Auto.py
    nave = Auto(motor, estanque, rueda1, rueda2, rueda3, rueda4, velocimetro)
    # Determinar el motor de la nave
    motor = nave.getMotor()
    # el estanque de la nave
    estanque = nave.getEstanque()
    # generar ruedas
    rueda1, rueda2, rueda3, rueda4 = nave.getRuedas()

    # llamar a funcion que permite en encendido y a la vez retornar
    # valor final del estanque aplicado el consumo
    estado, estanque = Encender(motor, estanque, rueda1, rueda2,
                                rueda3, rueda4)

    # ciclo hasta que se acabe el estanque o se apague el auto
    while final is True:

        # pedirle al usuario la velocidad que desea
        velocimetro = nave.getVelocimetro()

        # aplicar consumos segun tiempoy velocidad
        estanque = consumo_de_combustible(motor, estanque, velocimetro)

        # Aplicar desgaste de rueda
        rueda1, rueda2, rueda3, rueda4 = desgaste_rueda(rueda1, rueda2,
                                                        rueda3, rueda4)
        # mostar estado de vehiculo
        est_auto(motor, estanque, rueda1, rueda2, rueda3,
                 rueda4, velocimetro)

        # si auto esta sin estanque se detiene de lo contrario, segun lo 
        # quiera el usuario
        if estanque > 0:
            final = True

            estado = int(input("'9' para apagar/'0' seguir manejando\n"))
            if estado == 9:
                print("Adios mi rey, porga la alarma")
                final = False
            elif estado == 0:
                final = True
            else:
                print("Se va a cerrar el programa errorororororeoreo")
                break
        else:
            final = False
            print("Mi rey nos quedamos con el estanque vacio")





