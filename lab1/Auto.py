#! /usr/bin/env python3
# -*- coding:utf-8 -*-

from Motor import Motor
from Estanque import Estanque
from Ruedas import Ruedas
from Velocimetro import Velocimetro


class Auto():

    def __init__(self, motor, estanque, rueda1, rueda2,
                 rueda3, rueda4, velocimetro):
        # generacion de self a rtabajar
        self.motor = motor
        self.estanque = estanque
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4
        self.velocimetro = velocimetro
 
    # obtencion de motor segun la clase Motor(Motor.py)
    def getMotor(self):
        motor = Motor(self.motor)
        self.motor = motor.get_motor()
        return self.motor

    # obtencion de motor segun clase Estanque(Estanque.py)
    def getEstanque(self):
        estanque = Estanque(self.estanque)
        self.estanque = estanque.get_estanque()
        return self.estanque

    # geerar ruedad con sus valores iniciales en la clase Ruedas(Ruedas.py)
    def getRuedas(self):
        ruedas = Ruedas(self.rueda1, self.rueda2, self.rueda3, self.rueda4)
        rueda1, rueda2, rueda3, rueda4 = ruedas.get_ruedas()
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4
        return self.rueda1, self.rueda2, self.rueda3, self.rueda4

    # generar velocmeto y en caso que pase la normal de velocidad
    # pedir que la ingrese de nuevo
    # todo esto en clase Velocimetro(Velocimetro.py)
    def getVelocimetro(self):
        velocimetro = Velocimetro(self.velocimetro)
        self.velocimetro = velocimetro.get_velocimetro()
        if self.velocimetro > 120:
            print("No acelere tanto le puede costar la vida")
            self.velocimetro = velocimetro.get_velocimetro()
        else:
            return self.velocimetro




