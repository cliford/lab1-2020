#! /usr/bin/env python3
# -*- coding:utf-8 -*-


class Ruedas():
    # generacion de self individuales para cada rueda
    def __init__(self, rueda1, rueda2, rueda3, rueda4):
        self.rueda1 = rueda1
        self.rueda2 = rueda2
        self.rueda3 = rueda3
        self.rueda4 = rueda4

    def get_ruedas(self):
        # generar rueda nueva
        self.rueda1 = 100
        self.rueda2 = 100
        self.rueda3 = 100
        self.rueda4 = 100

        return self.rueda1, self.rueda2, self.rueda3, self.rueda4
