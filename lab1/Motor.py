#! /usr/bin/env python3
# -*- coding:utf-8 -*-
import random


class Motor():

    def __init__(self, motor):
        self.motor = motor

    # posibles cilindradas de los motores y dar valor a self.motor
    def get_motor(self):
        motores = ["1.2", "1.6"]
        self.motor = random.choice(motores)
        # print(self.motor)
        return self.motor

